#include <catch2/catch.hpp>

#include "cpputil/property.hpp"

#include <string>
#include <vector>

// TODO TEMPLATE_TEST_CASE
TEST_CASE("Property<int>", "[property]") {
  Property<int> p1;

  REQUIRE(!p1.changed());

  SECTION("Property::() sets initial value and should be unchanged") {
    Property<int> p2{123};
    REQUIRE(!p2.changed());
    REQUIRE(p2.get() == 123);
  }

  SECTION("Property#set sets value and marks as changed") {
    p1.set(42);
    REQUIRE(p1.changed());
    REQUIRE(p1.get() == 42);
    REQUIRE(p1.changed());

    SECTION("Property#reset resets changed to false while keeping value") {
      p1.reset();
      REQUIRE(p1.get() == 42);
      REQUIRE(!p1.changed());
    }

    SECTION("Property#get_and_reset gets value and resets while keeping value") {
      REQUIRE(p1.get_and_reset() == 42);
      REQUIRE(!p1.changed());
    }

    SECTION("Property copy constructor copies value but not changed status") {
      // NOLINTNEXTLINE(performance-unnecessary-copy-initialization)
      auto p2{p1};
      REQUIRE(p2.get() == 42);
      REQUIRE(!p2.changed());
    }

    SECTION("Property move constructor copies value but not changed status") {
      auto p2{std::move(p1)};
      REQUIRE(p2.get() == 42);
      REQUIRE(!p2.changed());
      // moved value should be defaulted
      // NOLINTNEXTLINE(bugprone-use-after-move, hicpp-invalid-access-moved)
      REQUIRE(p1.get() == int());
    }

    SECTION("Property copy assignment copies value and changes status to changed") {
      Property<int> p2;
      p2 = p1;
      REQUIRE(p2.get() == 42);
      REQUIRE(p2.changed());
    }

    SECTION("Property move assignment copies value and changes status to changed") {
      Property<int> p2{123};
      p2 = std::move(p1);
      REQUIRE(p2.get() == 42);
      REQUIRE(p2.changed());
      // moved value should be swapped
      // NOLINTNEXTLINE(bugprone-use-after-move, hicpp-invalid-access-moved)
      REQUIRE(p1.get() == 123);
    }
  }
}
