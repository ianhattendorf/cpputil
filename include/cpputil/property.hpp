#include <algorithm>

template <class T>
class Property {
public:
  Property() = default;
  Property(T value);
  ~Property() noexcept = default;

  // Copy constructor
  Property(const Property &other);

  // Move constructor
  Property(Property &&other) noexcept;

  // Copy assignment
  Property& operator=(const Property &other);

  // Move assignment
  Property& operator=(Property &&other) noexcept;

  bool changed() const;
  T get() const;
  T get_and_reset();
  void set(T value);
  void reset();
private:
  T value_;
  bool changed_ = false;
  // We need to track if we've been initialized, since we compare to
  // the previous value when calling set to determine if we've changed
  bool initialized_ = false;
};

template <class T>
Property<T>::Property(T value) : value_(value), initialized_(true) {}

template <class T>
Property<T>::Property(const Property<T> &other) : value_(other.value_), initialized_(true) {}

template <class T>
Property<T>::Property(Property<T> &&other) noexcept : value_(std::exchange(other.value_, T())), initialized_(true) {}

template <class T>
Property<T>& Property<T>::operator=(const Property &other) {
  if (this != &other) {
    set(other.value_);
    initialized_ = true;
  }
  return *this;
}

template <class T>
Property<T>& Property<T>::operator=(Property<T> &&other) noexcept {
  if (this != &other) {
    if (value_ != other.value_) {
      changed_ = true;
    }
    using std::swap;
    swap(value_, other.value_);
    initialized_ = true;
  }
  return *this;
}

template <class T>
bool Property<T>::changed() const {
  return changed_;
}

template <class T>
void Property<T>::set(T value) {
  if (initialized_ && value_ == value) {
    return;
  }

  value_ = value;
  changed_ = true;
}

template <class T>
T Property<T>::get() const {
  return value_;
}

template <class T>
void Property<T>::reset() {
  changed_ = false;
}

template <class T>
T Property<T>::get_and_reset() {
  reset();
  return get();
}
