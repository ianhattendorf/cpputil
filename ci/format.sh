#!/bin/sh

usage() {
  >&2 echo "Usage: $0 src-dir [check]"
  exit 1
}

if [ "$#" -lt 1 ] || [ "$#" -gt 2 ]; then
  usage
fi

src_files=`find "$1" -iname "*.hpp" -o -iname "*.cpp"`
echo -e "src_files:\n$src_files"

if [ "$#" -eq 2 ] && [ "$2" = 'check' ]; then
  echo "Checking formatting"
  if echo $src_files | xargs clang-format -style=file --output-replacements-xml | grep -c "<replacement " >/dev/null; then
    echo $src_files | xargs clang-format -style=file --output-replacements-xml -verbose
    >&2 echo "Formatting issues found"
    exit 1
  else
    echo "No formatting issues"
  fi
elif [ "$#" -eq 1 ]; then
  echo "Formatting files"
  echo $src_files | xargs clang-format -i -style=file
else
  usage
fi
